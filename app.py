from project import app
from project import db
db.create_all()

if __name__=='__main__':
    app.run(debug=True, port=5000)
