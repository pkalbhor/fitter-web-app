from flask import Flask, redirect, session, flash, request
from authlib.integrations.flask_client import OAuth
from authlib.jose import jwt
import json, jsonify
from flask import url_for, render_template
from flask_sqlalchemy import SQLAlchemy
import os, ast
from flask_bootstrap import Bootstrap

# Based on the official Authlib example for Google:
# https://github.com/authlib/demo-oauth-client/blob/master/flask-google-login/app.py

app = Flask(__name__)

# app.config.from_pyfile("app.cfg")
app.config['SQLALCHEMY_DATABASE_URI'] = 'sqlite:///userbase.db'
app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = False
app.config['CUSTOM_STATIC_PATH'] = 'userfiles/'
db = SQLAlchemy(app)

from project.auth.views import auth_blueprint
from project.package.views import fitter_blueprint
app.register_blueprint(auth_blueprint)
app.register_blueprint(fitter_blueprint, url_prefix='/fitter')

Bootstrap(app)
app.secret_key = os.urandom(24)

@app.route('/')
def root():
	return redirect(url_for('auth.index'))
