from flask_wtf import FlaskForm
from wtforms import StringField, SubmitField, SelectField
from wtforms.validators import DataRequired
from wtforms import widgets, SelectMultipleField

class MultiCheckboxField(SelectMultipleField):
    # widget = widgets.ListWidget(html_tag="ol", prefix_label=False)
    widget = widgets.TableWidget(with_table_tag=True)
    option_widget = widgets.CheckboxInput()

class NameForm(FlaskForm):
    # name = None # StringField('Which is your favorite place?', validators=[DataRequired()])
    SEQ_CHOICES = [('loadData', 'Load Data'), ('createplots', 'Create Plots')]
    SEQ_CHOICES.append(('fitEff', 'Fit efficiency distributions'))
    SEQ_CHOICES.append(('buildPdfs', 'Build PDF models'))
    SEQ_CHOICES.append(('fitSigM', 'Fit mass distribution (Signal)'))
    SEQ_CHOICES.append(('fitSigT', 'Fit decay time distribution (Signal)'))
    SEQ_CHOICES.append(('fitSigE', 'Fit decay time-error distribution (Signal)'))
    SEQ_CHOICES.append(('fitPeakM', 'Fit peaking mass distribution (Signal)'))
    SEQ_CHOICES.append(('fitPeakT', 'Fit peaking decay time distribution (Signal)'))
    seqKey = SelectField(u'Choose sequence to execute', default='buildPdfs', choices=SEQ_CHOICES)
    year = SelectField(u'Choose year of the data taking', choices=[2016, 2017, 2018], validators=[DataRequired()])
    plot_choices = [('plot_sigEfficiency', 'Signal efficiency distributions')]
    plot_choices += [('plot_peakEfficiency', 'Peak efficiency distribution'), ('plot_sigM', 'Bmass fit')]
    plot_choices += [('plot_sigT', 'Decay time distribution'), ('plot_sigE', 'Decay time Error')]
    plot_list = MultiCheckboxField('Select multiple options to plot the distributions', choices=plot_choices)
    # sequences = MultiCheckboxField('Label', choices=[('Choice1', 'Choice1'), ('Choice2', 'Choice2'), ('Choice3', 'Choice3')])
    submit = SubmitField('Submit')