import os, jsonify, glob, pdb
from flask import Blueprint, render_template, session, url_for, redirect, request, flash, g
from flask import send_from_directory, send_file, Response, stream_with_context
from werkzeug.security import safe_join
from .forms import NameForm
from functools import wraps
from shelljob import proc
from project.models import User
from project import db
from project import app

fitter_blueprint = Blueprint(
	'fitter',
	__name__,
	template_folder='templates',
	static_folder='static'
	)

from project.auth.views import oidc, login_required

gobal_process = proc.Group()

@fitter_blueprint.route('/', methods=['GET', 'POST'])
@login_required
def index():
	print(request.headers.get('User-Agent'))
	form = NameForm()
	if form.validate_on_submit() or request.method == 'POST':
		data = form.plot_list.data
		flash("Executing the sequence that you provided!")
		execute_sequences(form.data, plotsPath=session['USERFILES_PATH'])
	files = get_files()
	return render_template('index.html', user_name=session['USER_NAME'], form=form, files=files, proc=gobal_process)

def execute_sequences(form, plotsPath = None):
	form.pop('submit')
	form.pop('csrf_token')
	print(form)
	plot_list = ["effi"] if len(form['plot_list'])==0 else (" ").join(form['plot_list'])
	path = safe_join(app.root_path, 'package/fitter/LifetimeFitter/')
	cmd_list = [ "bash", "-c", "cd %s; python3 -u seqCollection.py"%path ]
	cmd_list[-1] += " -s %s" % form['seqKey']
	cmd_list[-1] += " --list %s" % (plot_list)
	cmd_list[-1] += " -y %s" % form['year']
	cmd_list[-1] += f" --pp {plotsPath}"
	cmd_list[-1] += "; cd -"
	print(cmd_list)
	# cmd_list = [ "bash", "-c", "for ((i=0;i<30;i=i+1)); do echo $i; sleep 1; done" ]
	p = gobal_process.run( cmd_list )

@fitter_blueprint.route('/plots')
@login_required
def plots():
	files = get_files()
	return render_template('plots.html', user_name=session['USER_NAME'], files=files)

@fitter_blueprint.route('/show/<path:filename>')
@login_required
def show_image(filename):
	return send_from_directory(session['USERFILES_PATH'], filename)

def get_files():
	os.chdir(session['USERFILES_PATH'])
	data = dict()
	for year in ['2016', '2017', '2018']:
		data[year] = dict()
		data[year]['Efficiency'] = glob.glob(f'plots_{year}/Efficiency/*.png')
		data[year]['SignalFits'] = glob.glob(f'plots_{year}/SignalFits/*.png')
	return data

# @fitter_blueprint.route( '/stream' )
# @login_required
# def stream():
# 	def read_process():
# 	    while gobal_process.is_pending():
# 		    lines = gobal_process.readlines()
# 		    for proc, line in lines:
# 			    yield line
# 	return Response( read_process(), mimetype= 'text/plain' )

# import eventlet
# eventlet.monkey_patch()

@fitter_blueprint.route( '/stream' )
def stream():
  def read_process():
    while gobal_process.is_pending():   
      lines = gobal_process.readlines()
      for proc, line in lines:
        yield "data:"+line.decode('UTF-8')+"\n\n"
  return Response( read_process(), mimetype= 'text/event-stream' )

@fitter_blueprint.route('/page')
def get_page():
    return render_template('page.html')
