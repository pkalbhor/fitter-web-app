$(document).ready(function() {
  $('.reveal').click(function() {
    if ($(this).text().trim() === 'Show') {
      $(this).html('Hide <i class="fa fa-chevron-circle-up">');
    }
    else {
      $(this).html('Show <i class="fa fa-chevron-circle-down">');
    }
  });
  
  $('#expand-collapse').click(function() {
    if ($(this).text().trim() === 'Expand all') {
      $('.content_section').slideDown();
      $('.reveal').html('Hide <i class="fa fa-chevron-circle-up">');
      $(this).html('Collapse all <i class="fa fa-chevron-circle-up">');
    }else {
      $('.content_section').slideUp();
      $('.reveal').html('Show <i class="fa fa-chevron-circle-down">');
      $(this).html('Expand all <i class="fa fa-chevron-circle-down">');
    }
  });

  // Show check-list for creating plots only when `createplots` options is selected
  if ($('#seqKey').val() != 'createplots') {
    $('#plot-checklist').hide();
  }
  $('#seqKey').change(function(){
    $('#plot-checklist').hide();
    if (this.value === 'createplots'){
      $('#plot-checklist').show();
    }
  });
  // ---------------------------------------------------------------------------
});

function toggle_display(idname){
  el = document.getElementById(idname);
  if(el.style.display == 'none'){
    el.style.display = 'block';
  }else{
    el.style.display = 'none';
  }
}